#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This script sends an email request for Tor Browser links to GetTor and then
# checks if it got a response.  Whenever GetTor fails to respond with links, we
# will get a monit alert.
#
# Run this script by adding it to monit's configuration file:
#
#   check program gettor-email-check with path "/path/to/gettor-email-check"
#     every 6 cycles
#     if status != 0 then alert
#
# Don't forget to add the Gmail password to the variable PASSWORD.

import os
import sys
import smtplib
import time
import imaplib
import email
import email.utils

# Standard Nagios return codes
OK, WARNING, CRITICAL, UNKNOWN = range(4)

FROM_EMAIL = "testbridgestorbrowser@gmail.com"
TO_EMAIL = "gettor@torproject.org"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT = 993

MESSAGE_FROM = TO_EMAIL
MESSAGE_BODY = "https://gitlab.com/thetorproject/"

PASSWORD = ""

# This will contain our test email's message ID.  We later make sure that this
# message ID is referenced in the In-Reply-To header of GetTor's response.
MESSAGE_ID = None


def log(*args, **kwargs):
    """
    Generic log function.
    """

    print("[+]", *args, file=sys.stderr, **kwargs)


def get_email_response():
    """
    Open our Gmail inbox and see if we got a response.
    """

    log("Checking for email response.")
    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    try:
        mail.login(FROM_EMAIL, PASSWORD)
    except Exception as e:
        return WARNING, str(e)

    mail.select("INBOX")

    _, data = mail.search(None, "ALL")
    email_ids = data[0].split()
    if len(email_ids) == 0:
        log("Found no response.")
        return CRITICAL, "No emails from GetTor found"

    return check_email(mail, email_ids)


def is_date_recent(date):
    """
    Return `True` if the email's `Date` header is recent-ish.
    """

    email_time = email.utils.mktime_tz(email.utils.parsedate_tz(date))
    our_time = time.time()
    diff = our_time - email_time
    # Was the email response sent within the last hour?
    return diff < (60 * 60)


def check_email(mail, email_ids):
    """
    Check if we got our expected email response.
    """

    log("Checking {:,} emails.".format(len(email_ids)))
    for email_id in email_ids:
        _, data = mail.fetch(email_id, "(RFC822)")

        # The variable `data` contains the full email object fetched by imaplib
        # <https://docs.python.org/3/library/imaplib.html#imaplib.IMAP4.fetch>
        # We are only interested in the response part containing the email
        # envelope.
        for response_part in data:
            if isinstance(response_part, tuple):
                m = str(response_part[1], "utf-8")
                msg = email.message_from_string(m)
                email_from = "{}".format(msg["From"])
                email_body = "{}".format(msg.as_string())
                email_date = "{}".format(msg["Date"])

                if (MESSAGE_FROM == email_from) and \
                   (MESSAGE_BODY in email_body) and \
                   is_date_recent(email_date):
                    mail.store(email_id, '+X-GM-LABELS', '\\Trash')
                    mail.expunge()
                    mail.close()
                    mail.logout()
                    log("Found correct response (referencing {})."
                        .format(MESSAGE_ID))
                    return OK, "GetTor's email responder works"
    mail.expunge()
    mail.close()
    mail.logout()
    log("Found no response.")
    return WARNING, "No emails from GetTor found"


def send_email_request():
    """
    Attempt to send a Tor Browser link request over Gmail.
    """

    subject = "Tor Browser"
    body = "windows en"

    log("Sending email.")
    global MESSAGE_ID
    MESSAGE_ID = email.utils.make_msgid(idstring="test-gettor",
                                        domain="gmail.com")
    email_text = "From: %s\r\nTo: %s\r\nMessage-ID: %s\r\nSubject: %s\r\n" \
                 "\r\n%s" % (FROM_EMAIL, TO_EMAIL, MESSAGE_ID, subject, body)

    try:
        mail = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        mail.login(FROM_EMAIL, PASSWORD)
        mail.sendmail(FROM_EMAIL, TO_EMAIL, email_text)
        mail.close()
        log("Email successfully sent (message ID: %s)." % MESSAGE_ID)
        return OK, "Sent email Tor Browser link request"
    except Exception as e:
        log("Error while sending email: %s" % err)
        return UNKNOWN, str(e)


if __name__ == "__main__":
    status, message = None, None

    # Send an email request to GetTor.

    try:
        status, message = send_email_request()
    except Exception as e:
        sys.exit(1)

    wait_time = 60 * 2
    log("Waiting %d seconds for a response." % wait_time)
    time.sleep(wait_time)

    # Check if we've received an email response.

    try:
        status, message = get_email_response()
    except KeyboardInterrupt:
        status, message = CRITICAL, "Caught Control-C..."
    except Exception as e:
        status = CRITICAL
        message = repr(e)
    finally:
        sys.exit(status)
